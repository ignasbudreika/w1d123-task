﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace AnagramSolver.ConsoleAppForDatabaseData
{
    public class DatabaseActions
    {
        public async void FillTable()
        {
            var words = ReadWordsFromAFile();

            using (SqlConnection connection = new SqlConnection("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= cf-anagram-solver-with-migrations; Integrated Security=True;"))
            {
                connection.Open();

                foreach (WordEntity word in await words)
                {
                    string saveWord = "INSERT into dbo.Words (Word,Category) VALUES (@word,@category)";

                    using (SqlCommand querySaveWord = new SqlCommand(saveWord, connection))
                    {
                        querySaveWord.Parameters.AddWithValue("@word", word.Word);
                        querySaveWord.Parameters.AddWithValue("@category", word.Category);

                        await querySaveWord.ExecuteNonQueryAsync();
                    }
                }

                connection.Close();
            }
        }
        private async static Task<List<WordEntity>> ReadWordsFromAFile()
        {
            var dictionary = new Dictionary<string, string>();
            var words = new List<WordEntity>();

            using (StreamReader reader = new StreamReader("zodynas.txt"))
            {
                string line;
                while ((line = await reader.ReadLineAsync()) != null)
                {
                    string[] parts = line.Split('\t');
                    if (!dictionary.ContainsKey(parts[0]))
                    {
                        dictionary.Add(parts[0], parts[1]);
                        words.Add(new WordEntity(parts[0], parts[1]));
                    }
                }
            }

            return words;
        }
        public async void ClearTable()
        {
            using (SqlConnection connection = new SqlConnection("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= codefirst-anagram-solver; Integrated Security=True;"))
            {
                connection.Open();

                string deleteWords = "DELETE FROM dbo.Word WHERE ID > 0";

                using (SqlCommand querySaveWord = new SqlCommand(deleteWords, connection))
                {
                    await querySaveWord.ExecuteNonQueryAsync();
                }

                connection.Close();
            }
        }
    }
}
