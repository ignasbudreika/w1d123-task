﻿using AnagramSolver.BusinessLogic;
using System;

namespace AnagramSolver.ConsoleAppForDatabaseData
{
    class Program
    {
        static void Main(string[] args)
        {
            DatabaseActions db = new DatabaseActions();
            db.FillTable();

            Console.ReadKey();
        }
    }
}
