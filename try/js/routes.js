﻿const routes = [
  {
    path: '/',
    template: '<h1>Home</h1>'
  },
  {
    path: '/words',
    template: '<h1>Words</h1>',
  },
  {
    path: '/add',
    template: '<h1>Add</h1>',
  },
];