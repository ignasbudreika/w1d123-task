﻿function generate_table() {
    // get the reference for the body
    var body = document.getElementsByTagName("body")[0];

    // creates a <table> element and a <tbody> element
    var tbl = document.createElement("table");
    var tblBody = document.createElement("tbody");

    // creates a table row
    var row = document.createElement("tr");

    var cell = document.createElement("td");
    var cellText = document.createTextNode(document.getElementById("InputWord").value);
    cell.appendChild(cellText);
    row.appendChild(cell);

    // add the row to the end of the table body
    tblBody.appendChild(row);

    // creates a table row
    var row = document.createElement("tr");

    var cell = document.createElement("td");
    var cellText = document.createTextNode(document.getElementById("InputCategory").value);
    cell.appendChild(cellText);
    row.appendChild(cell);

    // add the row to the end of the table body
    tblBody.appendChild(row);

    // put the <tbody> in the <table>
    tbl.appendChild(tblBody);
    // appends <table> into <body>
    body.appendChild(tbl);
    // sets the border attribute of tbl to 2;
    tbl.setAttribute("border", "2");

    var word = document.getElementById("InputWord").value;
    var category = document.getElementById("InputCategory").value;

    if (word.length > 0 && category.length > 0) {
        var url = "https://localhost:44379/api/word/add/" + word + '/' + category,
            xhttp = new XMLHttpRequest();

        xhttp.open('POST', url, true);
        xhttp.send();
    }
}