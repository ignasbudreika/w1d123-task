﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.CodeFirst.Repositories
{
    public class EFCachedWordRepository : ICachedWordRepository
    {
        private readonly AppDbContext _context;
        public EFCachedWordRepository(AppDbContext context)
        {
            _context = context;
        }

        public async Task CacheWord(CachedWordEntity cachedWord)
        {
            _context.CachedWords.Add(cachedWord);
            await _context.SaveChangesAsync();
        }

        public async Task ClearTable()
        {
            _context.CachedWords.RemoveRange(_context.CachedWords);
            await _context.SaveChangesAsync();
        }

        public async Task<string> GetCachedAnagrams(string word)
        {
            return (await _context
                .CachedWords
                .Where(x => x.Word.Equals(word))
                .Select(x => x.Ids)
                .FirstOrDefaultAsync());
        }

        public async Task<bool> IsCached(string word)
        {
            return (await _context
                .CachedWords
                .Where(x => x.Word.Equals(word))
                .FirstOrDefaultAsync()) is object;
        }

        public async Task RemoveCachedWords(string word, string id)
        {
            var toRemove = _context.CachedWords.Where(x => x.Word.Equals(word) || x.Ids.Contains(id));
            _context.CachedWords.RemoveRange(toRemove);
            await _context.SaveChangesAsync();
        }
    }
}
