﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.CodeFirst.Repositories
{
    public class EFLogRepository : ITableRepository, ILogRepository
    {
        private readonly AppDbContext _context;
        public EFLogRepository(AppDbContext context) : base("UserLog")
        {
            _context = context;
        }

        public async Task AddUserLog(UserLogEntity log)
        {
            _context.Logs.Add(log);
            await _context.SaveChangesAsync();
        }
        public async override Task ClearTable()
        {
            _context.Logs.RemoveRange(_context.Logs);
            await _context.SaveChangesAsync();
        }

        public async Task<int> GetLogCount(string ip, UserLogEntity.UserAction action)
        {
            return (await _context
                .Logs
                .Where(x => x.Ip.Equals(ip) && x.Action.Equals(action))
                .CountAsync());
        }

        public async Task<IQueryable<UserLogEntity>> GetUserLogs()
        {
            return (await _context.Logs.ToListAsync()).AsQueryable();

        }
    }
}
