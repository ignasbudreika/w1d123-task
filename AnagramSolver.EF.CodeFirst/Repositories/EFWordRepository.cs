﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.EF.CodeFirst.Repositories
{
    public class EFWordRepository : ITableRepository, IWordRepository
    {
        private readonly AppDbContext _context;
        public EFWordRepository(AppDbContext context) : base("Word")
        {
            _context = context;
        }

        public async Task AddWord(WordEntity word)
        {
            _context.Words.Add(word);
            await _context.SaveChangesAsync();
        }

        public async override Task ClearTable()
        {
            _context.Words.RemoveRange(_context.Words);
            await _context.SaveChangesAsync();
        }

        public async Task<int> Count()
        {
            return (await _context.Words.CountAsync());
        }

        public async Task DeleteWord(WordEntity word)
        {
            _context.Words.Remove(word);
            await _context.SaveChangesAsync();
        }

        public async Task<IQueryable<string>> GetAnagrams(List<int> ids)
        {
            return (await _context
                .Words
                .Where(x => ids.Contains(x.Id))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<int> GetId(string word)
        {
            return (await _context
                .Words
                .Where(x => x.Word.Equals(word))
                .Select(x => x.Id)
                .FirstOrDefaultAsync());
        }

        public async Task<IQueryable<int>> GetIDs(List<string> anagrams)
        {
            return (await _context
                .Words
                .Where(x => anagrams.Contains(x.Word))
                .Select(x => x.Id)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<WordEntity> GetWord(string word)
        {
            return (await _context
                .Words
                .Where(x => x.Word.Equals(word))
                .FirstOrDefaultAsync());
        }

        public async Task<IQueryable<WordEntity>> GetWords()
        {
            return (await _context
                .Words
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<IQueryable<string>> GetWords(int pageIndex, int pageSize)
        {
            return (await _context
                .Words
                .Where(x => (x.Id > (pageIndex - 1) * pageSize && x.Id < pageIndex * pageSize))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<IQueryable<string>> GetWords(string searchString)
        {
            return (await _context
                .Words
                .Where(x => x.Word.Contains(searchString))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task UpdateWord(WordEntity oldWord, WordEntity updated)
        {
            _context.Remove(oldWord);
            _context.Add(updated);
            await _context.SaveChangesAsync();
        }
    }
}
