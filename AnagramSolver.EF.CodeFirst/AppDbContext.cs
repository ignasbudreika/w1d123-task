﻿using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System.Configuration;

namespace AnagramSolver.EF.CodeFirst
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions options)
            : base(options)
        { }
        public AppDbContext()
        {
        }

        public DbSet<WordEntity> Words { get; set; }
        public DbSet<UserLogEntity> Logs { get; set; }
        public DbSet<CachedWordEntity> CachedWords { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                //optionsBuilder.UseSqlServer("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= cf-anagram-solver-with-migrations; Integrated Security=True;");
                optionsBuilder.UseSqlServer("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= anagram-solver-tests; Integrated Security=True;");
            }
        }
    }
}
