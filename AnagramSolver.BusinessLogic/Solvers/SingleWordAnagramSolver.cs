﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;

namespace AnagramSolver.BusinessLogic
{
    public class SingleWordAnagramSolver : IAnagramSolver
    {
        private readonly IWordRepository _wordRepository;
        public SingleWordAnagramSolver(IWordRepository wordRepository)
        {
            _wordRepository = wordRepository;
        }
        public async Task<IList<string>> GetAnagrams(string myWord)
        {
            IList<string> anagrams = new List<string>();

            string sorted = new string(myWord.OrderBy(c => c).ToArray());

            foreach (WordEntity word in (await _wordRepository.GetWords()))
            {
                if (Equals(sorted, word.Word))
                {
                    anagrams.Add(word.Word);
                    if (anagrams.Count == Settings.MaxCount)
                        return anagrams;
                }
            }
            return anagrams;
        }
        private bool Equals(string word1, string word2)
        {
            if(word1.Length == word2.Length)
            {
                if(word1.Equals(new Anagram(word2).Sorted))
                {
                    return true;
                }
            }
            else
            {
                return false;
            }
            return false;
        }
    }
}
