﻿using AnagramSolver.Contracts;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class HttpWordSolver : IAnagramSolver
    {
        private readonly HttpClient _client;

        public HttpWordSolver()
        {
            _client = new HttpClient();
        }
        
        public async Task<IList<string>> GetAnagrams(string word)
        {
            var response = _client.GetAsync("https://localhost:44379/api/anagram/" + word).Result;

            var content = response.Content.ReadAsStringAsync().Result;

            var words = JsonConvert.DeserializeObject<List<string>>(content);

            return words;
        }
    }
}
