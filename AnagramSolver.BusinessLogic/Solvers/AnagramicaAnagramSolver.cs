﻿using AnagramSolver.Contracts;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic.Solvers
{
    public class AnagramicaAnagramSolver : IAnagramSolver
    {
        private readonly HttpClient _client;

        public AnagramicaAnagramSolver()
        {
            _client = new HttpClient();
        }
        public async Task<IList<string>> GetAnagrams(string word)
        {
            var response = _client.GetAsync($"http://www.anagramica.com/all/{word}").Result;

            var content = response.Content.ReadAsStringAsync().Result;

            JObject jsonResponse = JObject.Parse(content);

            var words = jsonResponse["all"].Children().ToList();

            var anagrams = new List<string>();

            foreach(var anagram in words)
            {
                anagrams.Add(anagram.ToString());
            }

            return anagrams;
        }
    }
}