﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class SeveralWordsAnagramSolver : IAnagramSolver
    {
        private readonly List<WordEntity> Words;
        private int Max { get; set; }
        public SeveralWordsAnagramSolver(List<WordEntity> words, int max)
        {
            Words = words;
            Max = max;
        }
        public async Task<IList<string>> GetAnagrams(string myWords)
        {
            IList<string> anagrams = new List<string>();
            var usedWords = new List<string>();
            myWords = myWords.Replace(" ", "");

            for(int i = 0; i < Max; i++)
            {
                var frequency = GetCharFrequency(myWords);

                string anagram = FindRemainingWords(frequency, 0, usedWords);

                if(anagram.Replace(" ", "").Length == myWords.Length)
                {
                    anagrams.Add(anagram);
                    if(anagrams.Count == Max)
                    {
                        break;
                    }
                }
                else
                {
                    if (anagram.Equals(string.Empty))
                    {
                        break;
                    }
                    else
                    {
                        i--;
                    }
                }
            }

            return anagrams;
        }
        private Dictionary<char, int> GetCharFrequency(string str)
        {
            Dictionary<char, int> frequency = new Dictionary<char, int>();

            str = new string(str.OrderBy(c => c).ToArray());

            foreach(char c in str)
            {
                if (frequency.ContainsKey(c))
                {
                    frequency[c]++;
                }
                else
                {
                    frequency.Add(c, 1);
                }
            }

            return frequency;
        }
        private bool HasAllChars(Dictionary<char, int> inputFrequency, Dictionary<char, int> wordFrequency)
        {
            foreach(char c in wordFrequency.Keys)
            {
                if(inputFrequency.ContainsKey(c))
                {
                    if(inputFrequency[c] < wordFrequency[c])
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        private Dictionary<char, int> SubstractDictionaries(Dictionary<char, int> inputFrequency, Dictionary<char, int> wordFrequency)
        {
            foreach(char c in wordFrequency.Keys)
            {
                inputFrequency[c] -= wordFrequency[c];
            }
            return inputFrequency;
        }
        private string FindRemainingWords(Dictionary<char, int> frequency, int index, List<string> usedWords)
        {
            for(int i = index; i < Words.Count; i++)
            {
                if(!usedWords.Contains(Words[i].Word))
                {
                    Dictionary<char, int> wordFrequency = GetCharFrequency(Words[i].Word);

                    if (HasAllChars(frequency, wordFrequency))
                    {
                        frequency = SubstractDictionaries(frequency, wordFrequency);

                        if (frequency.Count == 0)
                        {
                            usedWords.Add(Words[i].Word);
                            return Words[i].Word;
                        }
                        else
                        {
                            usedWords.Add(Words[i].Word);
                            return Words[i].Word + " " + FindRemainingWords(frequency, i, usedWords);
                        }
                    }
                }
            }
            return "";
        }
    }
}
