﻿using Microsoft.Extensions.Configuration;

namespace AnagramSolver.BusinessLogic
{
    public static class Settings
    {
        public static int MaxCount { get; private set; }
        public static int MinWordLength { get; private set; }
        public static int MaxLogs { get; private set; }
        public static string DictionaryPath { get; private set; }
        public static string SqlConnection { get; private set; }
        public static string CodeFirstSql { get; private set; }
        public static string CodeFirstWithMigrations { get; private set; }
        public static string AnagramsPath { get; private set; }
        public static void GetSettings()
        {
            var configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

            MaxCount = int.Parse(configuration.GetSection("AppSettings").GetSection("MaxCount").Value);
            MinWordLength = int.Parse(configuration.GetSection("AppSettings").GetSection("MinWordLength").Value);
            DictionaryPath = configuration.GetSection("AppSettings").GetSection("FilePath").Value;
            SqlConnection = configuration.GetSection("AppSettings").GetSection("SqlConnection").Value;
            CodeFirstSql = configuration.GetSection("AppSettings").GetSection("CodeFirstSql").Value;
            MaxLogs = int.Parse(configuration.GetSection("AppSettings").GetSection("MaxLogs").Value);
            CodeFirstWithMigrations = configuration.GetSection("AppSettings").GetSection("CodeFirstSqlMigrations").Value;
            AnagramsPath = configuration.GetSection("AppSettings").GetSection("AnagramsPath").Value;
        }
    }
}
