﻿using AnagramSolver.Contracts;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using System.Data;
using AnagramSolver.EF.CodeFirst.Models;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic.Repositories
{
    public class DatabaseWordRepository : ITableRepository, IWordRepository
    {
        public DatabaseWordRepository(): base("Word")
        {
        }
        public async Task<int> Count()
        {
            using (SqlConnection connection = new SqlConnection(@Settings.SqlConnection))
            {
                connection.Open();

                string getCount = "SELECT COUNT(*) from dbo.Word";

                using (SqlCommand queryGetCount = new SqlCommand(getCount, connection))
                {
                    return (int)queryGetCount.ExecuteScalar();
                }
            }
        }

        public async Task<List<WordEntity>> GetWords()
        {
            using (SqlConnection connection = new SqlConnection(@Settings.SqlConnection))
            {
                var words = new List<WordEntity>();

                connection.Open();

                string getWords = "SELECT word, category FROM dbo.Word";

                using (SqlCommand queryGetWords = new SqlCommand(getWords, connection))
                {
                    using (var reader = (await queryGetWords.ExecuteReaderAsync()))
                    {
                        while (reader.Read())
                        {
                            words.Add(new WordEntity(reader["Word"].ToString(), reader["Category"].ToString()));
                        }
                        reader.Close();
                    }
                }

                return words;
            }
        }
        public async Task<List<string>> GetWords(int pageIndex, int pageSize)
        {
            using (SqlConnection connection = new SqlConnection(@Settings.SqlConnection))
            {
                var words = new List<string>();

                connection.Open();

                string getWords = "SELECT word FROM dbo.Word WHERE ID BETWEEN @from AND @to";

                using (SqlCommand queryGetWords = new SqlCommand(getWords, connection))
                {
                    queryGetWords.Parameters.AddWithValue("@from", (pageIndex - 1) * pageSize + 1);
                    queryGetWords.Parameters.AddWithValue("@to", pageIndex * pageSize);

                    using (var reader = (await queryGetWords.ExecuteReaderAsync()))
                    {
                        while (reader.Read())
                        {
                            words.Add(reader["Word"].ToString());
                        }
                        reader.Close();
                    }
                }

                return words;
            }
        }
        public async Task<List<string>> GetWords(string searchString)
        {
            using (SqlConnection connection = new SqlConnection(@Settings.SqlConnection))
            {
                var filteredWords = new List<string>();

                connection.Open();

                string getFilteredWords = "SELECT Word FROM dbo.Word WHERE CHARINDEX(@searchString, Word) > 0";

                using (SqlCommand queryGetFilteredWords = new SqlCommand(getFilteredWords, connection))
                {
                    queryGetFilteredWords.Parameters.AddWithValue("@searchString", searchString);

                    using (var reader = (await queryGetFilteredWords.ExecuteReaderAsync()))
                    {
                        while (reader.Read())
                        {
                            filteredWords.Add(reader["Word"].ToString());
                        }
                        reader.Close();
                    }
                }
                return filteredWords;
            }
        }
        public async Task<List<string>> GetAnagrams(List<int> ids)
        {
            using(SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                var anagrams = new List<string>();

                connection.Open();

                string getWord = "SELECT Word FROM dbo.Word WHERE Id = @id";

                using (SqlCommand queryGetWord = new SqlCommand(getWord, connection))
                {
                    foreach (int id in ids)
                    {
                        queryGetWord.Parameters.Clear();
                        queryGetWord.Parameters.AddWithValue("@id", id);

                        using (var reader = (await queryGetWord.ExecuteReaderAsync()))
                        {
                            reader.Read();
                            anagrams.Add(reader["Word"].ToString());
                        }
                    }
                }

                return anagrams;
            }
        }
        public async override Task ClearTable()
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                using (SqlCommand cmd = new SqlCommand("SP_CLEAR_TABLE", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Value = TableName;

                    connection.Open();
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }
        public async Task<List<int>> GetIDs(List<string> anagrams)
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                var ids = new List<int>();

                connection.Open();

                string getID = "SELECT Id FROM dbo.Word WHERE Word = @word";

                using (SqlCommand queryGetID = new SqlCommand(getID, connection))
                {
                    foreach (string anagram in anagrams)
                    {
                        queryGetID.Parameters.Clear();
                        queryGetID.Parameters.AddWithValue("@word", anagram);

                        using (var reader = (await queryGetID.ExecuteReaderAsync()))
                        {
                            reader.Read();
                            ids.Add(int.Parse(reader["Id"].ToString()));
                        }
                    }
                }

                return ids;
            }
        }

        Task<IQueryable<WordEntity>> IWordRepository.GetWords()
        {
            throw new System.NotImplementedException();
        }

        Task<IQueryable<string>> IWordRepository.GetWords(int pageIndex, int pageSize)
        {
            throw new System.NotImplementedException();
        }

        Task<IQueryable<string>> IWordRepository.GetWords(string searchString)
        {
            throw new System.NotImplementedException();
        }

        Task<IQueryable<int>> IWordRepository.GetIDs(List<string> anagrams)
        {
            throw new System.NotImplementedException();
        }

        Task<IQueryable<string>> IWordRepository.GetAnagrams(List<int> ids)
        {
            throw new System.NotImplementedException();
        }

        public Task AddWord(WordEntity word)
        {
            throw new System.NotImplementedException();
        }


        public Task DeleteWord(WordEntity word)
        {
            throw new System.NotImplementedException();
        }

        Task<int> IWordRepository.Count()
        {
            throw new System.NotImplementedException();
        }

        Task IWordRepository.ClearTable()
        {
            throw new System.NotImplementedException();
        }

        Task IWordRepository.AddWord(WordEntity word)
        {
            throw new System.NotImplementedException();
        }

        Task IWordRepository.UpdateWord(WordEntity oldWord, WordEntity newWord)
        {
            throw new System.NotImplementedException();
        }

        Task IWordRepository.DeleteWord(WordEntity word)
        {
            throw new System.NotImplementedException();
        }

        Task<WordEntity> IWordRepository.GetWord(string word)
        {
            throw new System.NotImplementedException();
        }

        public Task<int> GetId(string word)
        {
            throw new System.NotImplementedException();
        }
    }
}
