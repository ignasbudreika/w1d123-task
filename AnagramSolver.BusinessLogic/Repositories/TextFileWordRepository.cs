﻿using System;
using System.Collections.Generic;
using System.IO; 
using System.Linq;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;

namespace AnagramSolver.BusinessLogic
{
    public class TextFileWordRepository
    {
        private List<WordEntity> Words { get; set; }
        public TextFileWordRepository(string path)
        {
            Words = ReadWords(path);
        }
        private List<WordEntity> ReadWords(string path)
        {
            var dictionary = new Dictionary<string, string>();
            var words = new List<WordEntity>();

            using (StreamReader reader = new StreamReader(Settings.DictionaryPath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    string[] parts = line.Split('\t');
                    if (!dictionary.ContainsKey(parts[0]))
                    {
                        dictionary.Add(parts[0], parts[1]);
                        words.Add(new WordEntity(parts[0], parts[1]));
                    }
                }
            }

            return words;
        }
        public List<WordEntity> GetWords()
        {
            return Words;
        }
        public List<string> GetWords(int pageIndex, int pageSize)
        {
            return Words
                .Select(x => x.Word)
                .OrderBy(x => x)
                .AsQueryable()
                .Skip((pageIndex - 1) * pageSize)
                .Take(pageSize)
                .ToList();
        }
        public int Count()
        {
            return Words.Count();
        }

        public List<string> GetWords(string searchString)
        {
            throw new NotImplementedException();
        }

        public bool IsCached(string word)
        {
            throw new NotImplementedException();
        }

        public List<string> GetAnagrams(string word)
        {
            throw new NotImplementedException();
        }
        public List<int> GetIDs(List<string> anagrams)
        {
            throw new NotImplementedException();
        }
        public void ClearTable()
        {
            throw new NotImplementedException();
        }

        public List<string> GetAnagrams(List<int> ids)
        {
            throw new NotImplementedException();
        }

        public void AddWord(WordEntity word)
        {
            throw new NotImplementedException();
        }


        public void DeleteWord(WordEntity word)
        {
            throw new NotImplementedException();
        }

        public void UpdateWord(WordEntity oldWord, WordEntity newWord)
        {
            throw new NotImplementedException();
        }

        public WordEntity GetWord(string word)
        {
            throw new NotImplementedException();
        }

        public int GetId(string word)
        {
            throw new NotImplementedException();
        }
    }
}
