﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class LogRepository : ITableRepository, ILogRepository
    {
        public LogRepository() : base("UserLog")
        {
        }

        public async override Task ClearTable()
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                using (SqlCommand cmd = new SqlCommand("SP_CLEAR_TABLE", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Value = TableName;

                    connection.Open();
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }
        public async Task AddUserLog(UserLogEntity log)
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                connection.Open();

                string saveWord = "INSERT into dbo." + TableName + " (IP,Time,Word, Anagrams) VALUES (@ip,@time,@word,@anagrams)";

                using (SqlCommand querySaveWord = new SqlCommand(saveWord, connection))
                {
                    querySaveWord.Parameters.AddWithValue("@ip", log.Ip);
                    querySaveWord.Parameters.AddWithValue("@time", log.Time);
                    querySaveWord.Parameters.AddWithValue("@word", log.Word);
                    querySaveWord.Parameters.AddWithValue("@anagrams", string.Join(",", log.Anagrams));

                    await querySaveWord.ExecuteNonQueryAsync();
                }

                connection.Close();
            }
        }
        public async Task<List<UserLogEntity>> GetUserLogs()
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                var logs = new List<UserLogEntity>();

                connection.Open();

                string getUserLogs = "SELECT IP, Word, Time, Anagrams FROM dbo." + TableName;

                using (SqlCommand queryGetWords = new SqlCommand(getUserLogs, connection))
                {
                    using (var reader = (await queryGetWords.ExecuteReaderAsync()))
                    {
                        while (reader.Read())
                        {
                            logs.Add(new UserLogEntity(reader["IP"].ToString(), DateTime.Parse(reader["Time"].ToString()), reader["Word"].ToString(), reader["Anagrams"].ToString(), (UserLogEntity.UserAction)reader["Action"]));
                        }
                        reader.Close();
                    }
                }

                return logs;
            }
        }

        Task<IQueryable<UserLogEntity>> ILogRepository.GetUserLogs()
        {
            throw new NotImplementedException();
        }

        public Task<int> GetLogCount(string ip, UserLogEntity.UserAction action)
        {
            throw new NotImplementedException();
        }
    }
}
