﻿using AnagramSolver.Contracts;
using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using AnagramSolver.EF.CodeFirst;
using AnagramSolver.EF.CodeFirst.Models;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic.Repositories
{
    public class CachedWordRepository : ITableRepository, ICachedWordRepository
    {
        public CachedWordRepository() : base("CachedWord")
        {
        }
        public async override Task ClearTable()
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                using (SqlCommand cmd = new SqlCommand("SP_CLEAR_TABLE", connection))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@TableName", SqlDbType.VarChar).Value = TableName;

                    connection.Open();
                    await cmd.ExecuteNonQueryAsync();
                }
            }
        }
        public async Task<bool> IsCached(string word)
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                connection.Open();

                string isCached = "SELECT COUNT(*) FROM dbo." + TableName + " WHERE Word = @word";

                using (SqlCommand queryIsCached = new SqlCommand(isCached, connection))
                {
                    queryIsCached.Parameters.AddWithValue("@word", word);

                    if ((int)queryIsCached.ExecuteScalar() == 0)
                    {
                        return false;
                    }
                    return true;
                }
            }
        }
        public async Task CacheWord(CachedWordEntity cachedWord)
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                connection.Open();

                string cacheWord = "INSERT into dbo.CachedWord (Word,IDs) VALUES (@word,@ids)";

                StringBuilder IDs = new StringBuilder();

                using (SqlCommand queryCacheWord = new SqlCommand(cacheWord, connection))
                {
                    queryCacheWord.Parameters.AddWithValue("@word", cachedWord.Word);
                    queryCacheWord.Parameters.AddWithValue("@ids", cachedWord.Ids);

                    await queryCacheWord.ExecuteNonQueryAsync();
                }
            }
        }
        public async Task<string> GetCachedAnagrams(string word)
        {
            using (SqlConnection connection = new SqlConnection(Settings.SqlConnection))
            {
                connection.Open();

                string getIDs = "SELECT IDs FROM dbo.CachedWord WHERE Word = @word";

                using (SqlCommand queryGetIDs = new SqlCommand(getIDs, connection))
                {
                    queryGetIDs.Parameters.AddWithValue("@word", word);

                    string IDs;

                    using (var reader = (await queryGetIDs.ExecuteReaderAsync()))
                    {
                        reader.Read();
                        IDs = reader["IDs"].ToString();
                    }

                    return IDs;
                }
            }
        }

        public Task RemoveCachedWords(string word, string id)
        {
            throw new NotImplementedException();
        }
    }
}
