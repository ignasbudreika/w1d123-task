﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class CachedWordService : ICachedWordService
    {
        private readonly ICachedWordRepository _cachedWordRepository;
        public CachedWordService(ICachedWordRepository cachedWordRepository)
        {
            _cachedWordRepository = cachedWordRepository;
        }

        public async Task CacheWord(string word, string ids)
        {
            await _cachedWordRepository.CacheWord(new CachedWordEntity(word, ids));
        }

        public async Task ClearTable()
        {
            await _cachedWordRepository.ClearTable();
        }

        public async Task<List<int>> GetCachedAnagrams(string word)
        {
            return (await _cachedWordRepository
                    .GetCachedAnagrams(word))
                    .Split(',')
                    .Select(Int32.Parse)
                    .ToList();
        }

        public async Task<bool> IsCached(string word)
        {
            return (await _cachedWordRepository.IsCached(word));
        }

        public async Task RemoveCachedWords(string word, string id)
        {
            await _cachedWordRepository.RemoveCachedWords(word, id);
        }
    }
}
