﻿using System.IO;
using AnagramSolver.Contracts;

namespace AnagramSolver.BusinessLogic
{
    public class FileService : IFileService
    {
        public byte[] ReadDictionaryFile()
        {
            if (File.Exists(Settings.DictionaryPath))
            {
                byte[] bytes = File.ReadAllBytes(Settings.DictionaryPath);

                return bytes;
            }

            else return null;
        }
    }
}
