﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class WordService : IWordService
    {
        private readonly IWordRepository _wordRepository;
        private readonly ICachedWordRepository _cachedWordRepository;
        private readonly IAnagramSolver _solver;
        public WordService(IWordRepository wordRepository, IAnagramSolver solver, ICachedWordRepository cachedWordRepository)
        {
            _wordRepository = wordRepository;
            _solver = solver;
            _cachedWordRepository = cachedWordRepository;
        }
        public async Task<List<WordEntity>> GetWords()
        {
            return (await _wordRepository
                .GetWords())
                .ToList();
        }

        public async Task<PaginatedList<string>> GetWords(int pageIndex, int pageSize)
        {
            var totalWords = (await _wordRepository.Count());

            if (totalWords < pageSize * (pageIndex - 1))
            {
                pageIndex = (int)Math.Ceiling(totalWords / (double)pageSize);
            }
            if (pageIndex < 0)
            {
                pageIndex = 1;
            }

            var words = (await _wordRepository.GetWords(pageIndex, pageSize));

            return new PaginatedList<string>(words.ToList(), totalWords, pageIndex, pageSize);
        }
        public async Task<int> Count()
        {
            return (await _wordRepository.Count());
        }
        public async Task<PaginatedList<string>> GetWords(int pageIndex, int pageSize, string searchString)
        {
            var filteredWords = (await _wordRepository.GetWords(searchString)).ToList();

            if (filteredWords.Count < pageSize * (pageIndex - 1))
            {
                pageIndex = (int)Math.Ceiling(filteredWords.Count / (double)pageSize);
            }
            if (pageIndex < 0)
            {
                pageIndex = 1;
            }

            if((pageIndex - 1) * pageSize > filteredWords.Count)
            {
                pageIndex = 1;
            }
            if(filteredWords.Count - ((pageIndex - 1) * pageSize) < pageSize)
            {
                pageSize = filteredWords.Count - (pageIndex - 1) * pageSize;
            }

            var words = filteredWords.GetRange((pageIndex - 1) * pageSize, pageSize);

            return new PaginatedList<string>(words, filteredWords.Count, pageIndex, pageSize);
        }
        public async Task<List<string>> GetAnagrams(string word)
        {
            if (await _cachedWordRepository.IsCached(word))
            {
                var ids = (await _cachedWordRepository
                    .GetCachedAnagrams(word))
                    .Split(',')
                    .Select(Int32.Parse)
                    .ToList();
                return (await _wordRepository.GetAnagrams(ids)).ToList();
            }
            else
            {
                List<string> anagrams = await _solver.GetAnagrams(word) as List<string>;

                var ids = (await _wordRepository.GetIDs(anagrams));
                await _cachedWordRepository.CacheWord(new CachedWordEntity(word, string.Join(",", ids)));

                return anagrams;
            }
        }
        public async Task ClearTable()
        {
            await _wordRepository.ClearTable();
        }

        public async Task AddWord(string word, string category)
        {
            await _wordRepository.AddWord(new WordEntity(word, category));
        }

        public async Task<int> UpdateWord(string oldWord, string newWord)
        {
            var word = (await _wordRepository.GetWord(oldWord));
            var id = (await _wordRepository.GetId(oldWord));

            if (word != null)
            {
                await _wordRepository.UpdateWord(word, new WordEntity(newWord, word.Category));
            }

            return id;
        }

        public async Task<int> DeleteWord(string word)
        {
            var id = (await _wordRepository.GetId(word));

            await _wordRepository.DeleteWord(await _wordRepository.GetWord(word));

            return id;
        }
    }
}
