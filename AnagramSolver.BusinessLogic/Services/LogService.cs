﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst;
using AnagramSolver.EF.CodeFirst.Models;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.BusinessLogic
{
    public class LogService : ILogService
    {
        private readonly ILogRepository _logRepository;
        public LogService(ILogRepository logRepository)
        {
            _logRepository = logRepository;
        }

        public async Task AddUserLog(UserLogEntity log)
        {
            await _logRepository.AddUserLog(log);
        }

        public async Task<bool> Allowed(string ip)
        {
            if((await _logRepository.GetLogCount(ip, UserLogEntity.UserAction.Search)) 
                - (await _logRepository.GetLogCount(ip, UserLogEntity.UserAction.Add)) 
                - (await _logRepository.GetLogCount(ip, UserLogEntity.UserAction.Update)) 
                + (await _logRepository.GetLogCount(ip, UserLogEntity.UserAction.Delete)) >= Settings.MaxLogs)
            {
                return false;
            }
            return true;
        }

        public async Task ClearTable()
        {
            await _logRepository.ClearTable();
        }

        public async Task<UserLogList> GetUserLogs()
        {
            return new UserLogList() { Logs = (await _logRepository.GetUserLogs()).ToList() };
        }
    }
}
