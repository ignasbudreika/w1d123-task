﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace AnagramSolver.BusinessLogic
{
    public class EnumMapper
    {
        public enum Gender: int
        {
            Male = 1,
            Female = 2,
            Other = 3
        }
        public enum Weekday
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
        public static T MapValueToEnum<T, U>(U value) where T : struct, IConvertible
        {
            //T result;

            //result = (T)Enum.Parse(typeof(T), value);

            //if(!Enum.TryParse<T>(value.ToString(), out result))
            //{
            //    throw new Exception($"value '{value}' is not part of '{typeof(T)}' enum");
            //}
            try
            {
                T result = (T)Enum.Parse(typeof(T), value.ToString());
                if (Enum.IsDefined(typeof(T), result))
                    return result;
                else
                    throw new Exception($"value '{value}' is not part of '{typeof(T)}' enum");
            }
            catch (ArgumentException)
            {
                throw new Exception($"value '{value}' is not part of '{typeof(T)}' enum");
            }
        }
    }
}
