﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.BusinessLogic.Solvers;
using AnagramSolver.BusinessLogic.Repositories;
using AnagramSolver.ConsoleApp.Displays;
using AnagramSolver.Contracts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AnagramSolver.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings.GetSettings();

            Display display = new Display(PrintToConsole);
            DisplayWithEvents eventsDisplay = new DisplayWithEvents();
            UserInterface ui = new UserInterface();

            eventsDisplay.PrintInput += PrintToConsole;
            eventsDisplay.PrintInput += PrintToTextFile;

            IWordRepository wordRepository = new TextFileWordRepository(Settings.DictionaryPath);
            IAnagramSolver wordSolver = new SingleWordAnagramSolver(wordRepository);

            string word = ui.ReadWord();

            display._print($"Žodžio {word} anagramos:");

            foreach (string anagram in wordSolver.GetAnagrams(word))
            {
                display.FormattedPrint(Format, anagram);
                eventsDisplay.FormattedPrint(Format, anagram);
            }
        }
        private static string Format(string input)
        {
            return input.First().ToString().ToUpper() + input.Substring(1);
        }
        private static void PrintToConsole(string input)
        {
            //Console.WriteLine($"Žodžio {word} anagramos:");

            //foreach (string anagram in anagrams)
            //    Console.WriteLine(anagram);

            Console.WriteLine(input);
        }
        private static void PrintToDebug(string input)
        {
            //Debug.WriteLine($"Žodžio {word} anagramos:");

            //foreach (string anagram in anagrams)
            //    Debug.WriteLine(anagram);

            Debug.WriteLine(input);
        }
        private static void PrintToTextFile(string input)
        {
            using (StreamWriter writer = new StreamWriter(Settings.AnagramsPath, true))
            {
                //writer.WriteLine($"Žodžio {word} anagramos:");

                //foreach (string anagram in anagrams)
                //    writer.WriteLine(anagram);

                writer.WriteLine(input);
            }
        }
    }
}
