﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace AnagramSolver.ConsoleApp
{
    class UserInterface
    {
        public void ReadInput(IAnagramSolver singleWordSolver, IAnagramSolver severalWordsSolver)
        {   
            Console.WriteLine("Įveskite vieną ar daugiau žodžių:");
            string input = Console.ReadLine();

            if (input.Split(' ').Length == 1)
            {
                FindWordAnagrams(input, singleWordSolver);
            }
            else
            {
                FindSentenceAnagrams(input, severalWordsSolver);
            }
            Console.ReadKey();
        }
        private void FindWordAnagrams(string input, IAnagramSolver solver)
        {
            if (input.Length >= Settings.MinWordLength)
            {
                List<string> anagrams = solver.GetAnagrams(input) as List<string>;

                if(anagrams.Count > 0)
                {
                    Console.WriteLine("Žodžio " + input + " anagramos:");

                    foreach (string anagram in anagrams)
                        Console.WriteLine(anagram);
                }
                else
                {
                    Console.WriteLine("Žodis " + input + " anagramų neturi");
                }
            }
        }
        private static void FindSentenceAnagrams(string input, IAnagramSolver solver)
        {
            if (input.Split(' ').Length == Regex.Matches(input, @"[\S]" + "{" + Settings.MinWordLength + ",}").Count)
            {
                List<string> anagrams = solver.GetAnagrams(input) as List<string>;

                if(anagrams.Count > 0)
                {
                    Console.WriteLine("Sakinio " + input + " anagramos:");

                    foreach (string anagram in anagrams)
                        Console.WriteLine(anagram);
                }
                else
                {
                    Console.WriteLine("Sakinys " + input + " anagramų neturi");
                }
            }
        }
        public string ReadWord()
        {
            Console.WriteLine("Įveskite žodį:");
            string word = Console.ReadLine();

            return word;
        }
    }
}
