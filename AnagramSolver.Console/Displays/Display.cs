﻿using AnagramSolver.Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnagramSolver.ConsoleApp.Displays
{
    public class Display : IDisplay
    {
        public Action<string> _print;
        public Display(Action<string> print)
        {
            _print = print;
        }

        public void FormattedPrint(Func<string, string> format, string input)
        {
            _print(format(input));
        }
        //public delegate void Print(string input);
    }
}
