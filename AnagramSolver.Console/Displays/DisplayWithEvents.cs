﻿using AnagramSolver.Contracts.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnagramSolver.ConsoleApp.Displays
{
    public class DisplayWithEvents : IDisplay
    {
        public delegate void PrintEventHandler(string anagram);
        public event PrintEventHandler PrintInput;
        public void FormattedPrint(Func<string, string> format, string input)
        {
            PrintInput(format(input));
        }
    }
}
