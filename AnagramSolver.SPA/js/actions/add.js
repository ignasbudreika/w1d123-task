﻿function addWord() {
    var word = document.getElementById("InputWord").value;
    var category = document.getElementById("InputCategory").value;

    if (word.length > 0 && category.length > 0) {
        var url = "https://localhost:44379/api/word/add/" + word + '/' + category,
            xhttp = new XMLHttpRequest();

        xhttp.open('POST', url, true);
        xhttp.send();
    }
}