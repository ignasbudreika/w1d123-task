﻿using System;
using System.Collections.Generic;

namespace AnagramSolver.EF.DatabaseFirst
{
    public partial class CachedWord
    {
        public int Id { get; set; }
        public string Word { get; set; }
        public string Ids { get; set; }
    }
}
