﻿using System;
using System.Collections.Generic;

namespace AnagramSolver.EF.DatabaseFirst
{
    public partial class UserLog
    {
        public int Id { get; set; }
        public string Ip { get; set; }
        public string Time { get; set; }
        public string Word { get; set; }
        public string Anagrams { get; set; }
    }
}
