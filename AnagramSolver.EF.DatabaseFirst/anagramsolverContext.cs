﻿using System;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AnagramSolver.EF.DatabaseFirst
{
    public partial class anagramsolverContext : DbContext
    {
        public anagramsolverContext()
        {
        }

        public anagramsolverContext(DbContextOptions<anagramsolverContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CachedWordEntity> CachedWord { get; set; }
        public virtual DbSet<UserLogEntity> UserLog { get; set; }
        public virtual DbSet<WordEntity> Word { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= anagram-solver; Integrated Security=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CachedWordEntity>(entity =>
            {
                entity.Property(e => e.Ids)
                    .IsRequired()
                    .HasColumnName("IDs")
                    .IsUnicode(false);

                entity.Property(e => e.Word)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<UserLogEntity>(entity =>
            {
                entity.Property(e => e.Anagrams)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Ip)
                    .IsRequired()
                    .HasColumnName("IP")
                    .IsUnicode(false);

                entity.Property(e => e.Time)
                    .IsRequired()
                    .IsUnicode(false);

                entity.Property(e => e.Word)
                    .IsRequired()
                    .IsUnicode(false);
            });

            modelBuilder.Entity<WordEntity>(entity =>
            {
                entity.Property(e => e.Category).IsRequired();

                entity.Property(e => e.Word)
                    .IsRequired()
                    .HasColumnName("Word");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
