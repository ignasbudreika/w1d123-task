﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.DatabaseFirst.Repositories
{
    public class EFDLogRepository : ILogRepository
    {
        private readonly anagramsolverContext _context;
        public EFDLogRepository(anagramsolverContext context)
        {
            _context = context;
        }
        public async Task AddUserLog(UserLogEntity log)
        {
            _context.UserLog.Add(log);
            await _context.SaveChangesAsync();
        }

        public async Task ClearTable()
        {
            _context.UserLog.RemoveRange(_context.UserLog);
            await _context.SaveChangesAsync();
        }

        public async Task<int> GetLogCount(string ip, UserLogEntity.UserAction action)
        {
            return (await _context
                .UserLog
                .Where(x => x.Ip.Equals(ip) && x.Action.Equals(action))
                .CountAsync());
        }

        public async Task<IQueryable<UserLogEntity>> GetUserLogs()
        {
            return (await _context.UserLog.ToListAsync()).AsQueryable();
        }
    }
}
