﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.DatabaseFirst.Repositories
{
    public class EFDWordRepository : ITableRepository, IWordRepository
    {
        private readonly anagramsolverContext _context;
        public EFDWordRepository(anagramsolverContext context) : base("Word")
        {
            _context = context;
        }

        public async Task AddWord(WordEntity word)
        {
            _context.Word.Add(word);
            await _context.SaveChangesAsync();
        }

        public async override Task ClearTable()
        {
            _context.Word.RemoveRange(_context.Word);
            await _context.SaveChangesAsync();
        }

        public async Task<int> Count()
        {
            return (await _context.Word.CountAsync());
        }

        public async Task DeleteWord(WordEntity word)
        {
            _context.Word.Remove(word);
            await _context.SaveChangesAsync();
        }

        public async Task<IQueryable<string>> GetAnagrams(List<int> ids)
        {
            return (await _context
                .Word
                .Where(x => ids.Contains(x.Id))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<int> GetId(string word)
        {
            return (await _context
                .Word
                .Where(x => x.Word.Equals(word))
                .Select(x => x.Id)
                .FirstOrDefaultAsync());
        }

        public async Task<IQueryable<int>> GetIDs(List<string> anagrams)
        {
            return (await _context
                .Word
                .Where(x => anagrams.Contains(x.Word))
                .Select(x => x.Id)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<WordEntity> GetWord(string word)
        {
            return (await _context
                .Word
                .Where(x => x.Word.Equals(word))
                .FirstOrDefaultAsync());
        }

        public async Task<IQueryable<WordEntity>> GetWords()
        {
            return (await _context
                .Word
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<IQueryable<string>> GetWords(int pageIndex, int pageSize)
        {
            return (await _context
                .Word
                .Where(x => (x.Id > (pageIndex - 1) * pageSize && x.Id < pageIndex * pageSize))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task<IQueryable<string>> GetWords(string searchString)
        {
            return (await _context
                .Word
                .Where(x => x.Word.Contains(searchString))
                .Select(x => x.Word)
                .ToListAsync())
                .AsQueryable();
        }

        public async Task UpdateWord(WordEntity oldWord, WordEntity updated)
        {
            _context.Remove(oldWord);
            _context.Add(updated);
            await _context.SaveChangesAsync();
        }
    }
}
