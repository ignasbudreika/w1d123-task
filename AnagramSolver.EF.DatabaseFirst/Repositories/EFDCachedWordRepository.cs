﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.DatabaseFirst.Repositories
{
    public class EFDCachedWordRepository : ICachedWordRepository
    {
        private readonly anagramsolverContext _context;
        public EFDCachedWordRepository(anagramsolverContext context)
        {
            _context = context;
        }
        public async Task CacheWord(CachedWordEntity cachedWord)
        {
            _context.CachedWord.Add(cachedWord);
            await _context.SaveChangesAsync();
        }

        public async Task ClearTable()
        {
            _context.CachedWord.RemoveRange(_context.CachedWord);
            await _context.SaveChangesAsync();
        }

        public async Task<string> GetCachedAnagrams(string word)
        {
            return (await _context
                .CachedWord
                .Where(x => x.Word.Equals(word))
                .Select(x => x.Ids)
                .FirstOrDefaultAsync());
        }

        public async Task<bool> IsCached(string word)
        {
            return (await _context
                .CachedWord
                .Where(x => x.Word.Equals(word))
                .FirstOrDefaultAsync()) is object;
        }
        public async Task RemoveCachedWords(string word, string id)
        {
            var toRemove = _context.CachedWord.Where(x => x.Word.Equals(word) || x.Ids.Contains(id));
            _context.CachedWord.RemoveRange(toRemove);
            await _context.SaveChangesAsync();
        }
    }
}
