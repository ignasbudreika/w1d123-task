﻿using AnagramSolver.BusinessLogic;
using NUnit.Framework;
using Shouldly;
using System;

namespace AnagramSolver.Tests
{
    public class EnumMapperTests
    {
        [Test]
        public void MapValueToEnum_ParseGenderFromString_ReturnsGender()
        {
            //Arrange
            string value = "Male";

            //Act
            var genericResult = EnumMapper.MapValueToEnum<EnumMapper.Gender, string>(value);

            //Assert
            genericResult.ShouldBeOfType<EnumMapper.Gender>();
            genericResult.ShouldBe(EnumMapper.Gender.Male);
        }
        [Test]
        public void MapValueToEnum_ParseGenderFromInt_ReturnsGender()
        {
            //Arrange
            int value = 3;

            //Act
            var genericResult = EnumMapper.MapValueToEnum<EnumMapper.Gender, int>(value);

            //Assert
            genericResult.ShouldBeOfType<EnumMapper.Gender>();
            genericResult.ShouldBe(EnumMapper.Gender.Other);
        }
        [Test]
        public void MapValueToEnum_ParseWeekdayFromString_ReturnsTuesday()
        {
            //Arrange
            string value = "Tuesday";

            //Act
            var genericResult = EnumMapper.MapValueToEnum<EnumMapper.Weekday, string>(value);

            //Assert
            genericResult.ShouldBeOfType<EnumMapper.Weekday>();
            genericResult.ShouldBe(EnumMapper.Weekday.Tuesday);
        }
        [Test]
        public void MapValueToEnum_ParseFromNonExistingInt_ThrowsAnError()
        {
            //Arrange
            int value = 50;
            //Act
            //Assert
            Assert.Throws<Exception>(() => EnumMapper.MapValueToEnum<EnumMapper.Weekday, int>(value));
        }
        [Test]
        public void MapValueToEnum_ParseFromNonExistingSting_ThrowsAnError()
        {
            //Arrange
            string value = "Test";
            //Act
            //Assert
            Assert.Throws<Exception>(() => EnumMapper.MapValueToEnum<EnumMapper.Weekday, string>(value));
        }
    }
}
