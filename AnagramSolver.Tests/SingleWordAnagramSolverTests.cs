﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using NSubstitute;
using NSubstitute.Routing.Handlers;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;

namespace AnagramSolver.Tests
{
    class SingleWordAnagramSolverTests
    {
        [Test]
        public void GetAnagrams_EmptyDictionary_ReturnsEmptyStringList()
        {
            //Arrange
            var words = new List<WordEntity>();
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords().Returns(words);
            var wordRepo = Substitute.For<IWordRepository>();
            BusinessLogic.SingleWordAnagramSolver solver = new BusinessLogic.SingleWordAnagramSolver(wordRepo);

            //Act
            string word = "taip";
            List<string> anagrams = solver.GetAnagrams(word) as List<string>;

            //Assert
            anagrams.ShouldBeEmpty();
        }
        [Test]
        public void GetAnagrams_OnlyAnagramsInDictionary_ReturnsThreeWords()
        {
            //Arrange
            var words = new List<WordEntity>()
            {
                new WordEntity("taip","dkt"),
                new WordEntity("apti", "dkt"),
                new WordEntity("pati", "iv")
            };
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords().Returns(words);
            var wordRepo = Substitute.For<IWordRepository>();
            BusinessLogic.SingleWordAnagramSolver solver = new BusinessLogic.SingleWordAnagramSolver(wordRepo);

            //Act
            string word = "taip";
            List<string> anagrams = solver.GetAnagrams(word) as List<string>;

            //Assert
            anagrams.Count.ShouldBe(3);
        }
        [Test]
        public void GetAnagrams_MaxCountIs2_ReturnsOnlyTwoWords()
        {
            //Arrange
            var words = new List<WordEntity>()
            {
                new WordEntity("taip","dkt"),
                new WordEntity("apti", "dkt"),
                new WordEntity("pati", "iv")
            };
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords().Returns(words);
            var wordRepo = Substitute.For<IWordRepository>();
            BusinessLogic.SingleWordAnagramSolver solver = new BusinessLogic.SingleWordAnagramSolver(wordRepo);

            //Act
            string word = "taip";
            List<string> anagrams = solver.GetAnagrams(word) as List<string>;

            //Assert
            anagrams.Count.ShouldBe(2);
        }
        [Test]
        public void GetAnagrams_TwoAnagramsInDictionary_ReturnsListWithTwoWords()
        {
            //Arrange
            var words = new List<WordEntity>()
            {
                new WordEntity("taippat","dkt"),
                new WordEntity("apti", "dkt"),
                new WordEntity("netgi","vks"),
                new WordEntity("tarp", "vks"),
                new WordEntity("pati", "iv")
            };
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords().Returns(words);
            var wordRepo = Substitute.For<IWordRepository>();
            BusinessLogic.SingleWordAnagramSolver solver = new BusinessLogic.SingleWordAnagramSolver(wordRepo);

            //Act
            string word = "taip";
            List<string> anagrams = solver.GetAnagrams(word) as List<string>;

            //Assert
            anagrams.Count.ShouldBe(2);
        }
    }
}