﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using AnagramSolver.WebApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;

namespace AnagramSolver.Tests
{
    class WordsControllerTests
    {
        [Test]
        public void Index_EmptyWordRepo_ReturnsViewResult()
        {
            //Arrange
            var words = new List<WordEntity>();
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords(1, 100).Returns(new PaginatedList<string>(new List<string>(), 0, 0, 0));
            wordService.Count().Returns(0);
            var controller = new WordsController(null, wordService);

            //Act
            var result = controller.Index(1, string.Empty);

            //Assert
            result.ShouldBeOfType<ViewResult>();
        }
        [Test]
        public void Index_TwoWordsInList_ReturnedViewModelHas2Words()
        {
            //Arrange
            var wordService = Substitute.For<IWordService>();
            wordService.GetWords(1, 100).Returns(new PaginatedList<string>(new List<string> { "taip", "pati" }, 0, 0, 0));
            wordService.Count().Returns(5);
            var controller = new WordsController(null, wordService);

            //Act
            var result = controller.Index(1, string.Empty);

            //Assert
            var viewResult = result.ShouldBeOfType<ViewResult>();
            var model = viewResult.ViewData.Model.ShouldBeAssignableTo<PaginatedList<string>>();
            model.Count.ShouldBe(2);
        }
    }
}
