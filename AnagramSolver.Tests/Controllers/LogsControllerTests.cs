﻿using AnagramSolver.Contracts;
using AnagramSolver.WebApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnagramSolver.Tests.Controllers
{
    class LogsControllerTests
    {
        [Test]
        public void Index_EmptyLogsList_ReturnsViewResult()
        {
            //Arrange
            var logService = Substitute.For<ILogService>();
            var logger = Substitute.For<ILogger<LogsController>>();
            var controller = new LogsController(logger, logService);

            //Act
            var result = controller.Index();

            //Assert
            var viewResult = result.ShouldBeOfType<ViewResult>();
            var model = viewResult.ViewData.Model.ShouldBeAssignableTo<UserLogList>();
        }
    }
}
