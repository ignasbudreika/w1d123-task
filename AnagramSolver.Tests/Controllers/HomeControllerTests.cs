﻿using AnagramSolver.WebApp.Controllers;
using Microsoft.AspNetCore.Mvc;
using NUnit.Framework;
using Shouldly;
using System.Collections.Generic;
using NSubstitute;
using AnagramSolver.Contracts;
using System.Linq;
using AnagramSolver.EF.CodeFirst.Models;
using Castle.Core.Logging;
using Microsoft.Extensions.Logging;

namespace AnagramSolver.Tests
{
    class HomeControllerTests
    {
        [Test]
        public void Index_EmptyWordRepo_ReturnsViewResult()
        {
            //Arrange
            var words = new List<WordEntity>();
            var wordService = Substitute.For<IWordService>();
            var logService = Substitute.For<ILogService>();
            var cachedWordService = Substitute.For<ICachedWordService>();
            var logger = Substitute.For<ILogger<HomeController>>();
            wordService.GetWords().Returns(words);
            var controller = new HomeController(logger, wordService, logService, cachedWordService);
            string word = "";

            //Act
            var result = controller.Index(word);

            //Assert
            result.ShouldBeOfType<ViewResult>();
        }
        [Test]
        public void Blank_EmptyWordRepo_ReturnsViewResult()
        {
            //Arrange
            var wordService = Substitute.For<IWordService>();
            var logService = Substitute.For<ILogService>();
            var cachedWordService = Substitute.For<ICachedWordService>();
            var logger = Substitute.For<ILogger<HomeController>>();
            var controller = new HomeController(logger, wordService, logService, cachedWordService);

            //Act
            var result = controller.Blank();

            //Assert
            result.ShouldBeOfType<ViewResult>();
        }
    }
}
