﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnagramSolver.Tests.Services
{
    [TestFixture]
    public class WordsServiceTests
    {
        private IWordRepository _wordRepository;
        private ICachedWordRepository _cachedWordRepository;
        private IAnagramSolver _solver;
        private WordService _wordService;

        [SetUp]
        public void SetUp()
        {
            _wordRepository = Substitute.For<IWordRepository>();
            _cachedWordRepository = Substitute.For<ICachedWordRepository>();
            _solver = Substitute.For<IAnagramSolver>();
            _wordService = new WordService(_wordRepository, _solver, _cachedWordRepository);
        }

        [Test]
        public void GetWords_ShouldGetAllWords()
        {
            //Arrange
            int count = 3;
            _wordRepository.GetWords().Returns(new List<WordEntity>
            {
                new WordEntity(),
                new WordEntity(),
                new WordEntity(),
            }.AsQueryable());

            //Act
            var result = _wordService.GetWords();

            //Assert
            result.Count.ShouldBe(count);
        }
        [Test]
        public void GetWords_ShouldGetPaginatedList()
        {
            //Arrange
            int index = 1;
            int pageSize = 50;
            int count = 3;
            _wordRepository.GetWords(index, pageSize).Returns(new List<string>
            {
                "word1",
                "word2",
                "word2",
            }.AsQueryable());

            //Act
            var result = _wordService.GetWords(index, pageSize);

            //Assert
            result.Count.ShouldBe(count);
            result.PageIndex.ShouldBe(index);
            result.HasPreviousPage.ShouldBeFalse();
            result.HasNextPage.ShouldBeFalse();
        }
        [Test]
        public void Count_ShouldReturnThree()
        {
            //Arrange
            int count = 3;
            _wordRepository.Count().Returns(count);

            //Act
            var result = _wordService.Count();

            //Assert
            result.ShouldBe(count);
        }
        [Test]
        public void GetAnagrams_ShouldCallGetCachedAnagrams()
        {
            //Arrange
            string word = "word";
            _cachedWordRepository.IsCached(word).Returns(true);
            _cachedWordRepository.GetCachedAnagrams(word).Returns("1,1");
            _wordRepository.GetAnagrams(new List<int>() { 1, 1 }).Returns((new List<string>()).AsQueryable());

            //Act
            var result = _wordService.GetAnagrams(word);

            //Assert
            _cachedWordRepository.Received().GetCachedAnagrams(word);
        }
        [Test]
        public void GetAnagrams_ShouldUseSolver()
        {
            //Arrange
            string word = "word";
            _cachedWordRepository.IsCached(word).Returns(false);
            _solver.GetAnagrams(word).Returns(new List<string>());
            _wordRepository.GetIDs(new List<string>()).Returns(new List<int>() { 1, 1 }.AsQueryable());

            //Act
            var result = _wordService.GetAnagrams(word);

            //Assert
            _solver.Received().GetAnagrams(word);
        }
        [Test]
        public void UpdateWord_ShouldReturnId()
        {
            //Arrange
            string oldWord = "oldWord";
            string newWord = "newWord";
            int id = 1;
            _wordRepository.GetWord(oldWord).Returns(new WordEntity());
            _wordRepository.GetId(oldWord).Returns(id);

            //Act
            var result = _wordService.UpdateWord(oldWord, newWord);

            //Assert
            result.ShouldBe(id);
        }
        [Test]
        public void DeleteWord_ShouldReturnId()
        {
            //Arrange
            string word = "word";
            int id = 1;
            _wordRepository.GetWord(word).Returns(new WordEntity());
            _wordRepository.GetId(word).Returns(id);

            //Act
            var result = _wordService.DeleteWord(word);

            //Assert
            result.ShouldBe(id);
        }
    }
}
