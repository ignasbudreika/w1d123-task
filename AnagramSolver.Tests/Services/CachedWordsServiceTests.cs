﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using NSubstitute;
using NUnit.Framework;
using Shouldly;

namespace AnagramSolver.Tests.Services
{
    class CachedWordsServiceTests
    {
        [TestFixture]
        public class UserLogsServiceTests
        {
            private ICachedWordRepository _cachedWordRepository;
            private CachedWordService _cachedWordService;

            [SetUp]
            public void SetUp()
            {
                _cachedWordRepository = Substitute.For<ICachedWordRepository>();
                _cachedWordService = new CachedWordService(_cachedWordRepository);
            }

            [Test]
            public async void IsCached_ShouldBeTrue()
            {
                //Arrange
                string word = "word";
                _cachedWordRepository.IsCached(word).Returns(true);

                //Act
                var result = (await _cachedWordService.IsCached(word));

                //Assert
                result.ShouldBeTrue();
            }
            [Test]
            public async void IsCached_ShouldBeFalse()
            {
                //Arrange
                string word = "word";
                _cachedWordRepository.IsCached(word).Returns(false);

                //Act
                var result = (await _cachedWordService.IsCached(word));

                //Assert
                result.ShouldBeFalse();
            }
            [Test]
            public async void GetCachedAnagrams_ShouldReturnThree()
            {
                //Arrange
                int count = 3;
                string word = "word";
                string anagrams = "1,2,3";
                _cachedWordRepository.GetCachedAnagrams(word).Returns(anagrams);

                //Act
                var result = (await _cachedWordService.GetCachedAnagrams(word));

                //Assert
                result.Count.ShouldBe(count);
            }
        }
    }
}
