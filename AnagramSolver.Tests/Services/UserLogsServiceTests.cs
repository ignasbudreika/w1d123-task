﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using NSubstitute;
using NUnit.Framework;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AnagramSolver.Tests.Services
{
    [TestFixture]
    public class UserLogsServiceTests
    {
        private ILogRepository _userLogRepository;
        private LogService _userLogService;

        [SetUp]
        public void SetUp()
        {
            _userLogRepository = Substitute.For<ILogRepository>();
            _userLogService = new LogService(_userLogRepository);
        }

        [Test]
        public void GetUserLogs_ShouldGetAllUserLogs()
        {
            //Arrange
            _userLogRepository.GetUserLogs().Returns(new List<UserLogEntity>
            {
                new UserLogEntity("", DateTime.Now, "word1", "", UserLogEntity.UserAction.Search),
                new UserLogEntity("", DateTime.Now, "word2", "", UserLogEntity.UserAction.Search),
                new UserLogEntity("", DateTime.Now, "word3", "", UserLogEntity.UserAction.Search),
            }.AsQueryable());

            //Act
            var result = _userLogService.GetUserLogs();

            //Assert
            result.ShouldNotBeNull();
            result.Logs.Count().ShouldBe(3);

            _userLogRepository.Received().GetUserLogs();
        }
        [Test]
        public void Allowed_ShouldReturnTrue()
        {
            //Arrange
            string ip = "1";
            Settings.GetSettings();

            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Search).Returns(3);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Delete).Returns(1);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Add).Returns(1);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Update).Returns(1);

            //Act
            var result = _userLogService.Allowed(ip);

            //Assert
            result.ShouldBeTrue();
        }
        [Test]
        public void Allowed_ShouldReturnFalse()
        {
            //Arrange
            string ip = "1";
            Settings.GetSettings();

            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Search).Returns(3);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Delete).Returns(2);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Add).Returns(1);
            _userLogRepository.GetLogCount(ip, UserLogEntity.UserAction.Update).Returns(1);

            //Act
            var result = _userLogService.Allowed(ip);

            //Assert
            result.ShouldBeFalse();
        }
    }
}
