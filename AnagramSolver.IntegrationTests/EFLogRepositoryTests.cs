﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst;
using AnagramSolver.EF.CodeFirst.Repositories;
using Google.Protobuf.WellKnownTypes;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using NSubstitute;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Shouldly;
using AnagramSolver.EF.CodeFirst.Models;
using System.Linq;

namespace AnagramSolver.IntegrationTests
{
    public class EFLogRepositoryTests
    {
        private readonly ILogRepository _repository;
        public EFLogRepositoryTests()
        {
            Settings.GetSettings();
            var optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
            optionsBuilder.UseSqlServer("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= anagram-solver-test; Integrated Security=True;");
            var context = new AppDbContext(optionsBuilder.Options);
            _repository = new EFLogRepository(context);
        }
        [Test]
        public void GetUserLogs_ElevenLogs_ReturnsAllLogs()
        {
            //Arrange
            //int count = 11;

            ////Act
            //var result = _repository.GetUserLogs();

            ////Assert
            //result.Count.ShouldBe(count);
        }
    }
}
