using AnagramSolver.BusinessLogic;
using AnagramSolver.BusinessLogic.Repositories;
using AnagramSolver.Contracts;
using AnagramSolver.Contracts.Interfaces.WebService;
using AnagramSolver.EF.CodeFirst;
using AnagramSolver.EF.CodeFirst.Repositories;
using AnagramSolver.EF.DatabaseFirst;
using AnagramSolver.EF.DatabaseFirst.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SoapCore;
using System.ServiceModel;

namespace AnagramSolver.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            Settings.GetSettings();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddDbContext<AppDbContext>(options => options.UseSqlServer("Server= DESKTOP-5MQU21Q\\SQLEXPRESS; Database= anagram-solver-tests; Integrated Security=True;"));
            services.AddDbContext<AppDbContext>(options => options.UseSqlServer(Settings.CodeFirstWithMigrations));
            //services.AddScoped<IAnagramService, AnagramService>();
            services.AddScoped<IWordRepository, EFWordRepository>();
            services.AddScoped<IAnagramSolver, SingleWordAnagramSolver>();
            services.AddScoped<IWordService, WordService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<ILogRepository, EFLogRepository>();
            services.AddScoped<ICachedWordService, CachedWordService>();
            services.AddScoped<ICachedWordRepository, EFCachedWordRepository>();
            services.AddControllersWithViews();
            //services.AddSoapCore();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            //app.UseSoapEndpoint<IAnagramService>(path: "/AnagramService.svc", binding: new BasicHttpBinding());
            //app.UseSoapEndpoint<IAnagramService>("/AnagramService.asmx", new BasicHttpBinding(), SoapSerializer.XmlSerializer);

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "anagrams",
                    pattern: "/Anagrams/{word}",
                    defaults: new { controller ="Home", action = "Index" });
                endpoints.MapControllerRoute(
                    name: "blank",
                    pattern: "",
                    defaults: new { controller = "Home", action = "Blank" });
                endpoints.MapControllerRoute(
                    name: "words",
                    pattern: "{controller=Words}/{pageNumber}",
                    defaults: new { action = "Index" });
                endpoints.MapControllerRoute(
                    name: "log",
                    pattern: "{controller=Log}",
                    defaults: new { action = "Index" });
                endpoints.MapControllerRoute(
                    name: "remove",
                    pattern: "remove/{word}",
                    defaults: new { controller = "Home", action = "Remove" });
                endpoints.MapControllerRoute(
                    name: "addForm",
                    pattern: "{controller=Home}/addForm",
                    defaults: new { controller = "Home", action = "AddForm" });
                endpoints.MapControllerRoute(
                    name: "updateForm",
                    pattern: "update/{oldWord}",
                    defaults: new { controller = "Home", action = "UpdateForm" });
            });
        }
    }
}
