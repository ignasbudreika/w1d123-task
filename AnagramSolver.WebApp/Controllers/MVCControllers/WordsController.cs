﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AnagramSolver.Contracts;
using System.Threading.Tasks;

namespace AnagramSolver.WebApp.Controllers
{
    public class WordsController : Controller
    {
        private readonly ILogger<WordsController> _logger;
        private readonly IWordService _wordService;
        private const int PageSize = 100;

        public WordsController(ILogger<WordsController> logger, IWordService wordService)
        {
            _logger = logger;
            _wordService = wordService;
        }
        public async Task<IActionResult> Index(int pageNumber, string searchString)
        {
            ViewData["CurrentFilter"] = searchString;

            if (!String.IsNullOrEmpty(searchString))
            {
                return View(await _wordService.GetWords(pageNumber, PageSize, searchString));
            }
            else
            {
                return View(await _wordService.GetWords(pageNumber, PageSize));
            }       
        }
    }
}