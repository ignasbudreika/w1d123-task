﻿using AnagramSolver.Contracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace AnagramSolver.WebApp.Controllers
{
    public class LogsController : Controller
    {
        private readonly ILogger<LogsController> _logger;
        private readonly ILogService _logService;
        public LogsController(ILogger<LogsController> logger, ILogService logService)
        {
            _logger = logger;
            _logService = logService;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _logService.GetUserLogs());
        }
    }
}