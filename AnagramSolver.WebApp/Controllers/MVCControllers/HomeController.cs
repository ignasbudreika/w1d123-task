﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnagramSolver.WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IWordService _wordService;
        private readonly ILogService _logService;
        private readonly ICachedWordService _cachedWordService;

        public HomeController(ILogger<HomeController> logger, IWordService wordService, ILogService logService, ICachedWordService cachedWordService)
        {
            _logger = logger;
            _wordService = wordService;
            _logService = logService;
            _cachedWordService = cachedWordService;
        }

        public async Task<IActionResult> Index(string word)
        {
            var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

            if ((await _logService.Allowed(ip)))
            {
                var anagrams = _wordService.GetAnagrams(word);

                await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, word, string.Join(',', anagrams), UserLogEntity.UserAction.Search));

                return View(new AnagramView() { Word = word, Anagrams = (await _wordService.GetAnagrams(word)), Allowed = true });
            }

            return View(new AnagramView() { Word = word, Anagrams = new List<string>(), Allowed = false });
        }
        public async Task<IActionResult> Blank()
        {
            return View();
        }
        public async Task<IActionResult> Remove(string word)
        {
            var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

            await _cachedWordService.RemoveCachedWords(word, _wordService.DeleteWord(word).ToString());
            await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, word, "", UserLogEntity.UserAction.Delete));

            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddForm(string word, string category)
        {
            if(!string.IsNullOrEmpty(word) && !string.IsNullOrEmpty(category))
            {
                var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

                await _wordService.AddWord(word, category);
                await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, word, "", UserLogEntity.UserAction.Add));
            }

            return await Blank();
        }
        public async Task<IActionResult> AddForm()
        {
            return View();
        }
        public async Task<IActionResult> UpdateForm(string oldWord, string newWord)
        {
            ViewData["OldWord"] = oldWord;

            if (!string.IsNullOrEmpty(oldWord) && !string.IsNullOrEmpty(newWord))
            {
                var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

                await _cachedWordService.RemoveCachedWords(oldWord, _wordService.UpdateWord(oldWord, newWord).ToString());
                await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, newWord, "", UserLogEntity.UserAction.Update));
            }

            return View();
        }
    }
}
