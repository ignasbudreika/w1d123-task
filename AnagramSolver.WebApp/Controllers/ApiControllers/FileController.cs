﻿using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Mvc;
using AnagramSolver.Contracts;
using AnagramSolver.BusinessLogic;
using System.IO;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace AnagramSolver.WebApp.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IFileService _fileService;
        public FileController(IFileService fileService)
        {
            _fileService = fileService;
        }
        [HttpGet]
        public HttpResponseMessage GetFile()
        {
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);

            var bytes = _fileService.ReadDictionaryFile();

            var dataStream = new MemoryStream(bytes);

            if (bytes != null)
            {
                response.Content = new StreamContent(dataStream);

                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                response.Content.Headers.ContentDisposition.FileName = Settings.DictionaryPath;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response.Content.Headers.ContentLength = dataStream.Length;
            }
            else
            {
                response.StatusCode = HttpStatusCode.NotFound;
                response.ReasonPhrase = string.Format("File not found: {0} .", Settings.DictionaryPath);
            }

            return response;
        }
    }
}