﻿using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.WebApp.Controllers.ApiControllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WordController
    {
        private readonly IWordService _wordService;
        private readonly ILogService _logService;
        private const int PageSize = 100;

        public WordController(IWordService wordService, ILogService logService)
        {
            _wordService = wordService;
            _logService = logService;
        }
        [HttpGet("{pageNumber}/{searchString?}")]
        public async Task<PaginatedList<string>> GetPaginatedList([FromRoute] int pageNumber, [FromRoute] string searchString)
        {
            if (!String.IsNullOrEmpty(searchString))
            {
                return await _wordService.GetWords(pageNumber, PageSize, searchString);
            }
            else
            {
                return await _wordService.GetWords(pageNumber, PageSize);
            }
        }
        [HttpPost("add/{word}/{category}")]
        public async Task AddWord([FromRoute] string word, [FromRoute] string category)
        {
            if (!string.IsNullOrEmpty(word) && !string.IsNullOrEmpty(category))
            {
                var ip = (await System.Net.Dns.GetHostEntryAsync(System.Net.Dns.GetHostName())).AddressList[1].ToString();

                await _wordService.AddWord(word, category);
                await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, word, "", UserLogEntity.UserAction.Add));
            }
        }
    }
}
