﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using AnagramSolver.Contracts;
using AnagramSolver.EF.CodeFirst.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AnagramSolver.WebApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnagramController : ControllerBase
    {
        private readonly IWordService _wordService;
        private readonly ILogService _logService;

        public AnagramController(IWordService wordService, ILogService logService)
        {
            _wordService = wordService;
            _logService = logService;
        }

        [HttpGet("{word}")]
        public async Task<IList<string>> GetAnagrams([FromRoute] string word)
        {
            var ip = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

            if ((await _logService.Allowed(ip)))
            {
                var anagrams = (await _wordService.GetAnagrams(word));

                await _logService.AddUserLog(new UserLogEntity(ip, DateTime.Now, word, string.Join(',', anagrams), UserLogEntity.UserAction.Search));

                return anagrams;
            }

            return new List<string>();
        }
    }
}