﻿using AnagramSolver.Contracts.Interfaces.WebService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SOAPPractice
{
    public class Solver
    {
        public async void Solve()
        {
            Console.Write("Įveskite žodį: ");
            string word = Console.ReadLine();

            var binding = new BasicHttpBinding();
            var endpoint = new EndpointAddress(new Uri("http://localhost:44379/AnagramService.svc"));
            var channelFactory = new ChannelFactory<IAnagramService>(binding, endpoint);
            var serviceClient = channelFactory.CreateChannel();
            var anagrams = await serviceClient.Anagrams(word);

            foreach(string anagram in anagrams)
            {
                Console.WriteLine(anagram);
            }
        }
    }
}
