﻿using AnagramSolver.BusinessLogic;
using AnagramSolver.Contracts;
using AnagramSolver.Contracts.Interfaces.WebService;
using SOAPPractice.AnagramService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace SOAPPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calc = new Calculator();
            calc.Calculate();

            //Solver solver = new Solver();
            //solver.Solve();

            Console.ReadLine();
        }
    }
}
