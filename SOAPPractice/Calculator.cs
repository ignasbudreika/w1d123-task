﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SOAPPractice
{
    public class Calculator
    {
        CalculatorService.CalculatorSoapClient client;
        public Calculator()
        {
            client = new CalculatorService.CalculatorSoapClient();
        }
        public async void Calculate()
        {
            Console.Write("Pasirinkite veiksmą (+, -, *, /): ");
            char action = Char.Parse(Console.ReadLine().ToString());

            Console.Write("Įveskite pirmą skaičių: ");
            int first = int.Parse(Console.ReadLine());

            Console.Write("Įveskite antrą skaičių: ");
            int second = int.Parse(Console.ReadLine());

            double result;

            switch (action)
            {
                case '+':
                    result = await client.AddAsync(first, second);
                    break;
                case '-':
                    result = await client.SubtractAsync(first, second);
                    break;
                case '*':
                    result = await client.MultiplyAsync(first, second);
                    break;
                case '/':
                    result = await client.SubtractAsync(first, second);
                    break;
                default:
                    result = 0;
                    break;
            }

            Console.WriteLine($"Rezultatas: {result}");
        }
    }
}
