﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts.Interfaces.WebService
{
    [ServiceContract]
    public interface IAnagramService
    {
        [OperationContract]
        Task<List<string>> Anagrams(string word);
    }
}
