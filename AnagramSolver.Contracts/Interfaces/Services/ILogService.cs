﻿

using AnagramSolver.EF.CodeFirst.Models;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface ILogService
    {
        Task AddUserLog(UserLogEntity log);
        Task<UserLogList> GetUserLogs();
        Task ClearTable();
        Task<bool> Allowed(string ip);
    }
}
