﻿namespace AnagramSolver.Contracts
{
    public interface IFileService
    {
        byte[] ReadDictionaryFile();
    }
}
