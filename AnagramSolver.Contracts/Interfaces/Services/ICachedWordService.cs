﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface ICachedWordService
    {
        Task<bool> IsCached(string word);
        Task CacheWord(string word, string ids);
        Task<List<int>> GetCachedAnagrams(string word);
        Task ClearTable();
        Task RemoveCachedWords(string word, string id);
    }
}
