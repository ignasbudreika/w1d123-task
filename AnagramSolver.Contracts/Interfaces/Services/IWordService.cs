﻿using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface IWordService
    {
        Task<List<WordEntity>> GetWords();
        Task<PaginatedList<string>> GetWords(int pageIndex, int pageSize);
        Task<int> Count();
        Task<PaginatedList<string>> GetWords(int pageIndex, int pageSize, string searchString);
        Task<List<string>> GetAnagrams(string word);
        Task ClearTable();
        Task AddWord(string word, string category);
        Task<int> UpdateWord(string oldWord, string newWord);
        Task<int> DeleteWord(string word);
    }
}
