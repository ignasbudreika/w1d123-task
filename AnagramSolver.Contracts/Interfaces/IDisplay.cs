﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnagramSolver.Contracts.Interfaces
{
    //public delegate string Format(string word);
    public interface IDisplay
    {
        void FormattedPrint(Func<string, string> format, string input);
    }
}
