﻿using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public abstract class ITableRepository
    {
        protected string TableName { get; }
        public ITableRepository(string tableName)
        {
            TableName = tableName;
        }
        public abstract Task ClearTable();
    }
}
