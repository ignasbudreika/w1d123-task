﻿using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface ICachedWordRepository
    {
        Task<bool> IsCached(string word);
        Task CacheWord(CachedWordEntity cachedWord);
        Task<string> GetCachedAnagrams(string word);
        Task ClearTable();
        Task RemoveCachedWords(string word, string id);
    }
}
