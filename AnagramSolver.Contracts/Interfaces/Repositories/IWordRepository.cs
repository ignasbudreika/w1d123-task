﻿using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface IWordRepository
    {
        Task<IQueryable<WordEntity>> GetWords();
        Task<IQueryable<string>> GetWords(int pageIndex, int pageSize);
        Task<int> Count();
        Task<IQueryable<string>> GetWords(string searchString);
        Task<IQueryable<int>> GetIDs(List<string> anagrams);
        Task<IQueryable<string>> GetAnagrams(List<int> ids);
        Task<int> GetId(string word);
        Task ClearTable();
        Task AddWord(WordEntity word);
        Task UpdateWord(WordEntity oldWord, WordEntity newWord);
        Task DeleteWord(WordEntity word);
        Task<WordEntity> GetWord(string word);
    }
}
