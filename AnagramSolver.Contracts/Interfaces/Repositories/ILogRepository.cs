﻿using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnagramSolver.Contracts
{
    public interface ILogRepository
    {
        Task AddUserLog(UserLogEntity log);
        Task<IQueryable<UserLogEntity>> GetUserLogs();
        Task ClearTable();
        Task<int> GetLogCount(string ip, UserLogEntity.UserAction action);
    }
}
