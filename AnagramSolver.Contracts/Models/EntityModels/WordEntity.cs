﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.CodeFirst.Models
{
    public class WordEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; private set; }
        public string Word { get; private set; }
        public string Category { get; private set; }
        public WordEntity()
        {
        }

        public WordEntity(string word, string category)
        {
            Word = word;
            Category = category;
        }
    }
}
