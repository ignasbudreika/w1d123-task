﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnagramSolver.EF.CodeFirst.Models
{
    public class UserLogEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; private set; }
        public string Ip { get; private set; }
        public DateTime Time { get; private set; }
        public string Word { get; private set; }
        public string Anagrams { get; private set; }
        public UserAction Action { get; private set; }
        public UserLogEntity()
        {
        }
        public UserLogEntity(string ip, DateTime time, string word, string anagrams, UserAction action)
        {
            Ip = ip;
            Time = time;
            Word = word;
            Anagrams = anagrams;
            Action = action;
        }
        public enum UserAction
        {
            Search,
            Add,
            Delete,
            Update
        }
    }

}
