﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AnagramSolver.EF.CodeFirst.Models
{
    public class CachedWordEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }
        public string Word { get; set; }
        public string Ids { get; set; }
        public CachedWordEntity()
        {
        }

        public CachedWordEntity(string word, string ids)
        {
            Word = word;
            Ids = ids;
        }
    }
}
