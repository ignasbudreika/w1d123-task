﻿using System.Linq;

namespace AnagramSolver.Contracts
{
    public class Anagram
    {
        public string Word { get; private set; }
        public string Sorted { get; private set; }
        public Anagram(string word)
        {
            Word = word;
            Sorted = new string(word.OrderBy(c => c).ToArray());
        }
    }
}
