﻿using System;
using System.Collections.Generic;

namespace AnagramSolver.Contracts
{
    public class PaginatedList<T> : List<T>
    {
        public int PageIndex { get; private set; }
        public int TotalPages { get; private set; }
        public bool HasPreviousPage => (PageIndex > 1);
        public bool HasNextPage => (PageIndex < TotalPages);

        public PaginatedList(List<T> items, int totalWords, int pageIndex, int pageSize)
        {
            PageIndex = pageIndex;
            TotalPages = (int)Math.Ceiling(totalWords / (double)pageSize);

            this.AddRange(items);
        }
    }
}
