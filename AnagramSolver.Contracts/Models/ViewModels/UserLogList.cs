﻿using AnagramSolver.EF.CodeFirst.Models;
using System.Collections.Generic;

namespace AnagramSolver.Contracts
{
    public class UserLogList
    {
        public List<UserLogEntity> Logs { get; set; }
    }
}
