﻿using System.Collections.Generic;

namespace AnagramSolver.Contracts
{
    public class FilteredWordList
    {
        public string SearchString { get; set; }
        public List<string> Words { get; set; }
    }
}
