﻿using System.Collections.Generic;

namespace AnagramSolver.Contracts
{
    public class AnagramView
    {
        public string Word { get; set; }
        public List<string> Anagrams { get; set; }
        public bool Allowed { get; set; }
    }
}
